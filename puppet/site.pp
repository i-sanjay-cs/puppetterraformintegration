# puppet/web_server.pp

# Install Apache package
package { 'apache2':
  ensure => installed,
}

# Configure Apache service
service { 'apache2':
  ensure  => running,
  enable  => true,
}

# Define website content
file { '/var/www/html/index.html':
  ensure  => file,
  content => '<html><body><h1>Hello, Puppet!</h1></body></html>',
}
