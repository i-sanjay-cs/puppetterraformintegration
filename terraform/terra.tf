provider "google" {
  credentials = file("/home/master/terraform/key.json")
  project     = "terraworldline"
  region      = "us-central1"
}

resource "google_compute_instance" "example_instance" {
  name         = "example-instance2"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo apt install -y nginx",
    ]

    connection {
      type        = "ssh"
      user        = "poojaprajapati752"
      private_key = file("/home/master/.ssh/google_compute_engine")
      host        = google_compute_instance.example_instance.network_interface[0].access_config[0].nat_ip
    }
  }
}
